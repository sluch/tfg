# ALU 8-Bits

![Overview ALU](/images/ALU/ALUOverview.svg)


Nuestra ALU hará operaciones de 8 bits. Las operaciones que hará son:  
 · **Lógicas**: AND, OR, XOR  
 · **Aritmétricas**: ADD, SUB  

Para ello, dotaremos a nuestra ALU de 4 unidades funcionales.
Una se encargará de hacer los ANDs de 8 bits (8 puertas AND), otra de hacer
los ORs (8 puertas OR), y otra de hacer los XORs (8 puertas XOR). Finalmente
tendremos el sumador de 8 bits, que funcionará tanto como sumador como
restador.

![Diseño lógico de la ALU](/images/ALU/ALU.svg)

## Explicación de la ALU
### Sumador/restador de 8 bits:
Nuestro sumador/restador de 8 bits en la práctica sólo realiza sumas. Para 
realizar restas haremos uso de un truco: 
`A - B = A + (-B)`
Sabiendo esto, podemos convertir en negativo el operando B, y de esta forma
simplemente sumar. Para ello, tenemos que hacer el Complemento a 2 de B, que
consiste en invertir sus bits y sumarle 1. Invertimos sus bits gracias al grupo
de puertas XOR antes de la entrada del sumador, y aprovechamos la entrada de
control A2 para decidir si sumamos o restamos. Si sumamos, `A2 = 0`, por lo que
no sólo no se sumará 1 como Carry Input, sino que además, `B XOR 0 = B`, es decir,
no se modificará su valor. De lo contrario, si `A2 = 1`, significa que estamos
restando. Esto provoca que se añada 1 como Carry Input del sumador, y además
`B XOR 1 = ~B`, es decir, se invierten todos sus bits.

Para aclarar:  
1 xor 0 = 1 | -> Se conserva el valor (Suma)  
0 xor 0 = 0 |  

1 xor 1 = 0 | -> Se invierte el valor (Resta)  
0 xor 1 = 1 |

### Flag Overflow
El Flag de Overflow responde ante la fórmula:  
`R̅AB + RA̅B̅`, donde R, A y B son los bits más significativos del resultado y de
los inputs A y B respectivamente. Siguiendo esa fórmula, el desarrollo lógico
del flag overflow es trivial.

![Lógica del OF](/images/ALU/OverflowFlag.svg)


### Flag Zero
El Flag Zero se activa cuando el resultado es 0. Para poder activarlo como
corresponde, haremos un OR de todos los bits de resultado y luego negaremos
lo que salga. De esta forma, si algún bit del resultado es 1, el FZ será 0,
y viceversa.

### Flags Paridad y Signo
El Flag de Paridad se obtiene a partir del bit menos significativo del
resultado, y el de signo del más significativo.

## Señales de Control de la ALU
La ALU tendrá tres señales de control que llamaremos A2, A1 y A0, y estarán
asignadas a las operaciones siguientes:

A2 | A1 | A0 | op 
-- | -- | -- | --
0 | 0 | 0 | AND
0 | 0 | 1 | OR
0 | 1 | 0 | XOR
0 | 1 | 1 | ADD
1 | 0 | 1 | SUB



