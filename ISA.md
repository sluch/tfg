# Juego de Instrucciones v0.3

Leyenda:  
 **Rd**: Registro destino  
 **Rr**: Registro fuente  
 **Rs**: Registro fuente ¿2?  
 **dir**: dirección de memoria  
 **inm:**: inmediato  
 **offset**: desplazamiento  

## Juego de 13 instrucciones
### Instrucciones aritmético-lógicas
Tipo | Mnemónico | Operandos | Operación | Descripción
---- | --------- | --------- | --------- | -----------
R | ADD | Rd, Rr, Rs | Rd ← Rr + Rs |
R | SUB | Rd, Rr, Rs | Rd ← Rr - Rs |
R | CMP | Rr, Rs | Rr - Rs | Activa Flags
R | AND | Rd, Rr, Rs | Rd ← Rr · Rs |
R | OR | Rd, Rr, Rs | Rd ← Rr v Rs |
R | XOR | Rd, Rr, Rs | Rd ← Rr ⊕ Rs |

![Instrucciones Tipo R](images/insts/instsR.svg)


### Instrucciones de transferencia de datos
Tipo | Mnemónico | Operandos | Operación | Descripción
---- | --------- | --------- | --------- | -----------
I | LD | Rd, dir | Rd ← (dir) |
I | LDI | Rd, inm | Rd ← inm[7:0] |
I | ST | Rd, dir | dir ← Rd |
I | STI | Rd, inm | (Rd) ← inm[7:0] |
R | MOV | Rd, Rr | Rd ← Rr |

![Instrucciones Tipo I](images/insts/instsI.svg)




### Instrucciones de salto
Tipo | Mnemónico | Operandos | Operación | Descripción
---- | --------- | --------- | --------- | -----------
J | JMP | dir | PC ← dir |
J | BEQ | dir | Si FZ=1 PC ← dir |

![Instrucciones Tipo J](images/insts/instsJ.svg)
