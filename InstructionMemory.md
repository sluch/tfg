# Memoria de Instrucciones
La memoria de instrucciones tiene una composición particular.
Las instrucciones tienen una longitud de 16 bits, así que podemos hacer dos
cosas:
* 1. Hacer una memoria direccionable a byte (8 bits).
* 2. Hacer una memoria direccionable a palabra de 16 bits.

## Memoria direccionable a byte:
Ejemplo de una memoria de 4 insts. direccionable a byte (8 celdas de 8 bits).

Instrucción | Dirección
 :--------: | :-------:
 0 | 0 |
 0 | 1 |
 1 | 2 |
 1 | 3 |
 2 | 4 |
 2 | 5 |
 3 | 6 |
 3 | 7 |

El campo de dirección necesitaría 3 bits. Sin embargo, debido a que nuestras
instrucciones son de 16 bits, nunca apuntaríamos a las direcciones impares.
Por esto, podríamos utilizar un Shift Left 1 en el campo inmediato de las
instrucciones `JMP` y `BEQ` para así impedir el direccionamiento a posiciones
impares que resultarían en una instrucción errónea siendo Fetcheada.

![Memoria direccionable a byte](/images/InstructionMemory/EsquemaMemoria1.svg)

## Memoria direccionable a 16 bits:
Una memoria de 4 insts. direccionable a byte (4 celdas de 16 bits).

No hace falta que los registros sean de 16 bits, podemos usar registros
de 8 bits igualmente.

![Memoria direccionable a 16 bits](/images/InstructionMemory/EsquemaMemoria2.svg)
No sólo es más simple el diseño de la memoria, sino que además nos ahorramos un
SL1 en el diseño de la ruta de datos.