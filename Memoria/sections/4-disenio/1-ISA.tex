Lo primero a realizar durante el desarrollo de una arquitectura de computadoras es
el juego o repertorio de instrucciones. Determinar el número de instrucciones,
la longitud, los modos de direccionamiento, el tipo de almacenamiento, etc., ya
que todos estos factores condicionarán el diseño del resto de componentes. Así,
si se desconoce el modo de direccionamiento de las instrucciones, no se podrá 
determinar cómo se envían los operandos a memoria, o cómo se accede al banco de
registros, independientemente de la estructura de éstos otros componentes.
Para hacer esto, primeramente se decidirán las instrucciones que queremos que
conformen el repertorio, teniendo en cuenta que cuanto mayor sea el número
de instrucciones, mayor será la complejidad del hardware.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Elección de las instrucciones que conforman el repertorio} 
\label{subsubsec:EleccionISA}
Es importante recordar la clasificación de instrucciones en tres grupos: 
instrucciones aritmético-lógicas, instrucciones de transferencia e instrucciones
de control de flujo, así se deberán incorporar instrucciones de cada grupo y tener un
repertorio variado. Como nota aparte, no entra dentro de los objetivos de este
proyecto probar la completitud de Turing del repertorio, sino que la daremos
por garantizada basándonos en que tenemos instrucciones de lectura y escritura
de memoria, de salto condicional y de operaciones aritmético-lógicas\cite{MOVTuring} \cite{URISC}.



Así, las instrucciones del repertorio serán:
\begin{itemize}
    \item \textbf{Instrucciones aritmético-lógicas:}
        \textit{ADD, SUB, CMP, AND, OR, XOR}.\\
        Lo primero que se puede observar es la ausencia de instrucción \textit{NOT}.
        La ausencia de esta instrucción obedece a motivos de simplicidad: como
        operación lógica no tiene mayor complejidad, e implementarla en hardware
        añadiría complicaciones en la codificación de las instrucciones y en la
        ALU. Además, es fácil invertir los bits sin utilizar la instrucción
        \textit{NOT}: En complemento a dos, la operación para invertir una cadena
        \emph{x} sería \emph{NOT}\(x = -x -1\).
    \item \textbf{Instrucciones de transferencia de datos:} 
        \textit{LD, LDI, ST, STI, MOV}.\\
        Son especialmente relevantes las instrucciones LDI y STI, que cargan en
        registro o en memoria un valor inmediato, respectivamente.
        \hyperref[subsubsec:C_ISA]{Más adelante},
        cuando se detalle el formato de las instrucciones, quedará claro por qué
        sin estas instrucciones no se podrían inicializar valores ni en registros
        ni en memoria, debido al formato del resto de las instrucciones.
    \item \textbf{Instrucciones de salto:}
        \textit{JMP, BEQ}.\\
        Una instrucción de salto incondicional y otra de salto condicional.
        Incluyendo estas instrucciones garantizamos el mínimo control de flujo
        necesario para implementar lógica variada.
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Decisiones sobre el repertorio}
Antes de diseñar el juego de instrucciones puede resultar útil determinar algunos
factores sobre la arquitectura que afectarán al diseño del repertorio.


\paragraphtitle{Longitud de la codificación de las instrucciones}
Es importante preguntarse, antes de todo lo demás, cuál será el tamaño de las
instrucciones de nuestro repertorio, ya que este factor será determinante en
otras decisiones, como en el número de registros de nuestro sistema o la
cantidad de celdas de memoria direccionables.

Para simplificar el diseño y mantener una coherencia respecto al número reducido
de instrucciones en nuestro repertorio, se utilizará una codificación de longitud
fija. En este punto hay dos alternativas principales: instrucciones de 8 o de 16 bits.
Con cualquiera de las dos longitudes, 4 bits se dedicarán a la codificación de las
13 instrucciones.

Con instrucciones de 8 bits sólo quedarían otros 4 para codificar inmediatos
e índices de registros.
Como se verá en la siguiente sección, se utilizarán dos registros fuente y
un registro destino en muchas instrucciones, por lo que contar con sólo 4 bits para
codificar tres registros resulta insuficiente. Aunque se utilizase un registro fuente
y uno destino, esta opción obligaría a contar con sólo dos bits para cada
índice, reduciendo el número de registros direccionables en nuestro sistema a 4. Queda claro
que la única opción viable es la de utilizar instrucciones de más de 8 bits de longitud.
La opciónde 16 bits es la más sencilla, dejando 12 bits para indicar índices a registros,
inmediatos o direcciones.


\paragraphtitle{Tipo de almacenamiento de los operandos}
Manteniendo siempre la intención de que esta arquitectura pueda servir como
refuerzo o referencia para estudiantes, el foco del diseño debe estar en que
sea sencillo de entender, lo más cercano posible a las estructuras que usamos
los seres humanos en el día a día. Por esto, el almacenamiento de operandos se
realizó con \textbf{registros de propósito general}. Inicialmente las instrucciones
aceptarían dos operandos (uno de los operandos fuente sería sobreescrito con el
resultado de la operación), pero finalmente se usaron tres operandos fuente
para que la sintaxis de las instrucciones fuera más parecida al
lenguaje natural. Aparte de esto, la arquitectura mantendrá un enfoque de 
\textbf{carga-almacenamiento}, siendo sus instrucciones de tipo
\textbf{registro-registro}, es decir, para operar con datos éstos tendrán que
encontrarse en registros. No se podrá operar con datos ubicados en la memoria
sin antes llevarlos a registros con alguna instrucción de carga como
\textit{LD} o \textit{LDI}.


\paragraphtitle{Tipo y tamaño de los operandos}
Al ser una arquitectura de 8 bits, los datos con los que se opere principalmente
serán de 8 bits. Siendo el objetivo el aprendizaje, el tratamiento de números
en coma flotante o datos vectoriales se salen del alcance. Por esto, las
instrucciones operarán sobre datos de tipo entero de 8 bits en complemento a dos.


\paragraphtitle{Número de registros}
Muchas arquitecturas cuentan con un número limitado de registros. Por ejemplo,
\textbf{x86} cuenta sólamente con 8 registros de propósito general, incluyendo
\textit{EBP} y \textit{ESP}, que son rara vez utilizados por el programador.
\textbf{MIPS32} cuenta con 32 registros, y otras arquitecturas más reducidas como
\textbf{AVR} de 8 bits también cuentan con 32 registros de propósito general,
además de los de propósito especial~\cite{ATMELISA}.

La arquitectura descrita contará con 8 registros de propósito general, R0-R7, por
lo que se necesitarán 3 bits para referenciarlos.


\paragraphtitle{Tratamiento de las instrucciones de control de flujo}
El problema principal sobre estas instrucciones es su modo de direccionamiento,
pensando sobretodo en cómo conseguir que direccionasen al mayor número de
celdas de memoria posible.
Sin embargo, debido a la naturaleza minimalista de la arquitectura, ninguna de sus memorias
será excesivamente amplia. Por ello, las instrucciones de salto no se complicarán
y su modo de direccionamiento será inmediato.
Como se puede ver en la figura~\ref{fig:instsJ}, las instrucciones de salto tendrán
12 bits de direccionamiento, lo que significa que podrán direccionar a $2^{12}=4096$
posiciones de memoria. En una memoria con posiciones de 8 bits, ésto serían 2048
instrucciones, mientras que en una memoria con posiciones de 16 bits como la que
se usará en este proyecto, serían 4096 instrucciones.

Aunque la arquitectura permite este direccionamiento, en la implementación final
las memorias seŕan mucho más reducidas y no utilizarán todos los bits de éste
tipo de instrucción.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Repertorio de instrucciones} \label{Repertorio}
El repertorio de instrucciones final cuenta con 13 instrucciones:\\
\\
\textsc{Leyenda}\\
    \textbf{Rd}: Registro destino\\
    \textbf{Rr, Rs}: Registro fuente\\
    \textbf{dir}: dirección de memoria\\
    \textbf{inm}: inmediato\\
    \textbf{(dir)}: Contenido de la dirección de memoria \textit{dir}.\\


\paragraphtitle{Instrucciones aritmético-lógicas}
\begin{center}
    \begin{tabular}{ |c|c|c|c|c| }
    \hline
        Tipo & Mnemónico & Operandos & Operación & Modos de direccionamiento \\ \hline
        \rowcolor{gris}
        R & ADD & Rd, Rr, Rs & Rd $\gets$ Rr + Rs & Registro \\ \hline
        R & SUB & Rd, Rr, Rs & Rd $\gets$ Rr - Rs & Registro \\ \hline
        \rowcolor{gris}
        R & CMP & Rr, Rs     & Rr - Rs & Registro \\ \hline
        R & AND & Rd, Rr, Rs & Rd $\gets$ Rr $\land$ Rs & Registro \\ \hline
        \rowcolor{gris}
        R & OR  & Rd, Rr, Rs & Rd $\gets$ Rr $\lor$ Rs & Registro \\ \hline
        R & XOR & Rd, Rr, Rs & Rd $\gets$ Rr $\oplus$ Rs & Registro \\ 
    \hline
    \end{tabular}
    \captionof{table}{Instrucciones aritmético-lógicas.}
\end{center}

\begin{center}
    \centering
    \includesvg{images/4-disenio/insts/instsR}
    \captionof{figure}{Formato de las instrucciones de tipo R.}
\end{center}



\paragraphtitle{Instrucciones de transferencia de datos}
\begin{center}
    \begin{tabular}{ |c|c|c|c|c| }
    \hline
        Tipo & Mnemónico & Operandos & Operación & Modos de direccionamiento \\ \hline
        \rowcolor{gris}
        I & LD  & Rd, dir & Rd $\gets$ (dir) & Registro, directo \\ \hline
        I & LDI & Rd, inm & Rd $\gets$ inm[7:0] & Registro, inmediato \\ \hline
        \rowcolor{gris}
        I & ST  & Rd, dir & dir $\gets$ Rd & Inmediato, registro \\ \hline
        I & STI & Rd, inm & (Rd) $\gets$ inm[7:0] & Indirecto, inmediato \\ \hline
        \rowcolor{gris}
        R & MOV & Rd, Rr  & Rd $\gets$ Rr & Registro \\
    \hline
    \end{tabular}
    \captionof{table}{Instrucciones de transferencia.}
\end{center}

\begin{center}
    \centering
    \includesvg{images/4-disenio/insts/instsI}
    \captionof{figure}{Formato de las instrucciones de tipo I.}
\end{center}

El motivo por el que las instrucciones \textit{LDI} y \textit{STI} aceptan como
segundo operando \textit{inm[7:0]} es porque los registros son de 8 bits, y
en el campo inmediato se escriben 9 bits. Por eso se trunca el valor
introducido en el campo inmediato para que quepa en el registro destino.


\paragraphtitle{Instrucciones de salto}
\begin{center}
    \centering
    \begin{tabular}{ |c|c|c|c|c| }
    \hline
        Tipo & Mnemónico & Operandos & Operación & Modos de direccionamiento \\ \hline
        \rowcolor{gris}
        J & JMP & dir & PC $\gets$ dir & inmediato \\ \hline
        J & BEQ & dir & Si FZ=1 PC $\gets$ dir & inmediato \\
    \hline
    \end{tabular}
    \captionof{table}{Instrucciones de salto.}
\end{center}

\begin{center}
    \centering
    \includesvg{images/4-disenio/insts/instsJ}
    \captionof{figure}{Formato de las instrucciones de tipo J.}
    \label{fig:instsJ}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Características del juego de instrucciones} \label{subsubsec:C_ISA}
Las 13 instrucciones que forman parte del repertorio condicionarán aspectos de diseño
de la arquitectura tanto como el formato de las mismas.
En primer lugar, se resumirán los modos de direccionamiento utilizados. Luego se
expondrán algunas consideraciones.

\paragraphtitle{Modos de direccionamiento soportados}
Los modos de direccionamiento utilizados son:
\begin{itemize}
    \item \textbf{Inmediato}
        En muchas instrucciones, los operandos se codificarán directamente en
        la instrucción. Este modo es sencillo de entender y de implementar
        pero tiene limitaciones y no es lo más interesante para
        los compiladores, ya que obliga que el campo inmediato esté calculado
        en tiempo de compilación y esto no siempre es posible. En muchas
        ocasiones resultaría más útil que el operando se encontrase en un registro
        fijo (que sí se puede saber en tiempo de compilación), y cuando se averigüe
        el valor del inmediato en tiempo de ejecución, guardarlo en el registro
        correspondiente. Es una solución más versátil.
    \item \textbf{Registro}
        Este modo es muy práctico y suele utilizarse en la mayoría de repertorios
        debido a su naturalidad y sencillez de implementación.
    \item \textbf{Directo}
        Este modo se utiliza en dos instrucciones de transferencia de datos, por
        su utilidad y facilidad de implementación.
    \item \textbf{Indirecto}
        Es utilizado en la instrucción \textit{STI}. Resulta práctico y su
        implementación no se complica demasiado.
\end{itemize}
Este repertorio no se ha diseñado con la ortogonalidad como objetivo ni ha entrado
en consideración durante ninguna etapa de diseño.


\paragraphtitle{Memoria direccionable}
Para analizar esto podemos dividir en dos tipos las instrucciones con acceso a
memoria: las que acceden a memoria de instrucciones, y la que lo hacen a la de
datos.
\begin{itemize}
    \item \textbf{Instrucciones que acceden a memoria de datos:}
        En esta categoría se encuentran \textit{LD}, \textit{ST} y \textit{STI}.
        En el caso de \textit{LD} y \textit{ST}, se utiliza el campo de
        inmediato/dirección para acceder a memoria, lo que nos da 9 bits
        de direccionamiento, es decir, $2^{9}=512$ direcciones accesibles. En
        el caso de \textit{STI}, accedemos a memoria por el contenido del registro
        \textit{Rd}, de 8 bits, por lo que podemos acceder a $2^{8}=256$
        posiciones de memoria.

    \item \textbf{Instrucciones que acceden a memoria de instrucciones:}
        En esta categoría entran \textit{JMP} y \textit{BCE}. Ya que ambas
        modifican el registro Contador de Programa de la misma forma, esto es,
        con el valor del campo inmediato de 12 bits, podrán direccionar directamente
        $2^{12}=4096$ posiciones de la memoria de instrucciones.


\end{itemize}

\paragraphtitle{Instrucciones \textit{LDI} y \textit{STI}}
La importancia de estas instrucciones es crucial, ya que sin ellas no podríamos de ninguna
forma introducir datos en registro o en memoria. Las instrucciones \textit{LD} y \textit{ST}
permiten cargar en un registro un dato que se encuentra en memoria, o guardar en memoria un
dato que se encuentra en un registro, pero inicialmente tanto la memoria como los registros
carecen de contenido. Es por ello que para poder operar con datos, se necesita la forma
de introducir datos de manera explícita, y esto se logra con inmediatos.
En otras arquitecturas también se pueden inicializar los valores de los registros o de la
memoria mediante operaciones aritmético-lógicas. Por ejemplo, podríamos realizar la
operación \textit{ADD EAX 0 1} para inicializar el valor del registro \textit{EAX} a 1, pero
las operaciones aritmético-lógicas de esta arquitectura no aceptan tampoco inmediatos, por lo
que no sirve esta aproximación.


\paragraphtitle{Efectos a nivel de compilación}
El diseño de una arquitectura de computadores tendrá un inmenso efecto sobre el compilador
encargado de traducir código de alto nivel al ensamblador propio de dicha arquitectura.
Es por eso que determinadas estructuras y funcionalidades en el repertorio de instrucciones
facilitarán o dificultarán el trabajo al compilador.

\begin{itemize}
    \item \textbf{Registros de propósito general}
        Los registros de propósito general son, por lo general, más eficientes y preferidos
        que sus alternativas, ya sean por acumulador o por stack. Debido en gran parte a que
        permiten un mayor manejo que las otras dos alternativas, sobretodo que el stack, que
        no permite accesos aleatorios. Pero principalmente debido a que los registros son
        varios órdenes de magnitud más rápidos que la memoria\cite{GPR} \cite{DEAC}.
        
    \item \textbf{Instrucciones de salto}
        Ambas instrucciones de salto utilizan un inmediato como modo de direccionamiento.
        Esta condición impone serias limitaciones al diseño del compilador, ya que
        necesitará saber la dirección destino del salto en tiempo de compilación.
\end{itemize}

