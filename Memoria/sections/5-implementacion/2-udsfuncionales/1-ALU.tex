La Unidad Aritmético Lógica (UAL o ALU) es el componente hardware en el que se
realizan las operaciones aritméticas y lógicas necesarias para la correcta ejecución
de cada instrucción.
En la arquitectura descrita se llevarán a cabo las operaciones de suma, resta, y
\textit{and}, \textit{or} y \textit{xor} lógicos. Como ya se ha explicado,
se utilizarán tres señales de control para indicar cuál de estas operaciones
se va a realizar con los operandos introducidos.

\begin{figure}[H]
    \centering
    \begin{tabular} { |c c c|c| }
    \hline
        \multicolumn{3}{|c}{ALUOp$_{[2:0]}$} \vline & Operación \\ \hline
        \rowcolor{gris}
        0 & 0 & 0 & \textit{AND} \\ \hline
        0 & 0 & 1 & \textit{OR}  \\ \hline
        \rowcolor{gris}
        0 & 1 & 0 & \textit{XOR} \\ \hline
        0 & 1 & 1 & \textit{ADD} \\ \hline
        \rowcolor{gris}
        1 & 0 & 1 & \textit{SUB} \\
    \hline
    \end{tabular}
    \captionof{table}{Señal de control \textit{ALUOpX} y operaciones que realiza.}
\end{figure}

Para realizar el diseño completo de la ALU serán necesarios componentes concretos:
\begin{itemize}
    \item \textbf{AND de 8 bits:}
        Para realizar la operación \textit{AND} sobre dos operandos de 8 bits.
    \item \textbf{OR de 8 bits:}
        Para realizar la operación \textit{OR} sobre dos operandos de 8 bits.
    \item \textbf{XOR de 8 bits:}
        Para realizar la operación \textit{XOR} sobre dos operandos de 8 bits.
    \item \textbf{Sumador de 8 bits:}
        Para realizar las instrucciones \textit{ADD}, \textit{SUB} y \textit{CMP}.
    \item \textbf{Enabler XOR de 8 bits:}
        Para poder restar se aprovechará que $A-B=A+(-B)$. Se hará el complemento
        a dos del operando \textit{B} gracias al Enabler XOR y a la entrada de
        acarreo del sumador de 8 bits.
    \item \textbf{x3 Multiplexores de 2 a 1:}
        Para poder escoger entre todas las operaciones que hace la ALU con los
        operandos y enviarlas a la salida \textit{Res}.
    \item \textbf{Puertas lógicas varias:}
        Para activar correctamente las banderas de cada operación.
\end{itemize}

Una vista a más bajo nivel del esquemático de la ALU se encuentra en la
figura~\ref{fig:ALU}, en la que \textit{[MSB]} hace referencia al bit más significativo
(\textit{Most Significant Bit}), es decir, el de más a la izquierda. \textit{[LSB]}
hace referencia al menos significativo (\textit{Least Significant Bit}),
el de más a la derecha.\\
\begin{figure}[H]
    \hspace*{-1cm}
    \includesvg[scale=0.5]{images/5-implementacion/1-ALU/ALU}
    \caption{ALU}
    \label{fig:ALU}
\end{figure}

\newpage
La lógica de la parte inferior es la encargada de activar correctamente la bandera de
overflow.
\begin{figure}[H]
    \vspace*{-0.9cm}
    \centering
    $OF=(R\overbar{A}\overbar{B})\lor(\overbar{R}AB)$\\
    \vspace{0.3cm}
    \includesvg{images/5-implementacion/1-ALU/OverflowFlag}
    \caption{Overflow Flag}
\end{figure}
\vspace*{-0.2cm}

Esta ALU opera con datos de ocho bits y tiene la capacidad de realizar aritmética sobre
entero tanto con y sin signo. El programador tendrá la responsabilidad de interpretar las
banderas correspondientes, los rangos de los operandos y el resultado, atendiendo a que
se cumplen las siguientes propiedades, entre otras:
\begin{itemize}
    \item Sobre las flags:
        \begin{itemize}
            \item Overflow:
                Sólo cobra sentido en operaciones con signo.
            \item Carry:
                Sólo cobra sentido en operaciones sin signo.
        \end{itemize}
    \item Sobre los rangos (siendo \textit{n} el número de bits, 8 en este caso):
        \begin{itemize}
            \item Sin signo:
                $[0, 2^{n}-1]$, es decir: $[0, 255]$.
            \item Con signo (complemento a dos):
                $[-2^{n-1}, 2^{n-1}-1]$, es decir: $[-128, 127]$.
        \end{itemize}
\end{itemize}

\begin{figure}[H]
    \centering
    \hspace*{-2cm}
    \includegraphics[scale=0.5]{images/5-implementacion/1-ALU/ALUSimSigned.png}
    \caption{Simulación de las operaciones aritméticas con signo}
\end{figure}

\begin{figure}[H]
    \centering
    \hspace*{-2cm}
    \includegraphics[scale=0.5]{images/5-implementacion/1-ALU/ALUSimUnsigned.png}
    \caption{Simulación de las operaciones aritméticas sin signo}
\end{figure}


La batería de pruebas para las operaciones aritméticas es la mostrada en la
tabla~\ref{bateria}, en la que los números entre
corchetes son la representación con signo del valor a su izquierda.

\begin{figure}[H]
    \begin{tabular} { |c|c|c|c c c|c|c|c|c| }
    \hline
        A & Op & B & Res & & CF & ZF & PF & OF & SF \\ \hline
        \rowcolor{gris}
        0 & + & 0  & 0 & & 0 & 1 & 0 & 0 & 0 \\ \hline
        0 & - & 0  & 0 & & 1 & 1 & 0 & 0 & 0 \\ \hline
        \rowcolor{gris}
        0 & + & 1  & 1  & & 0 & 0 & 1 & 0 & 0 \\ \hline
        0 & - & 1  & 255[-1] & & 0 & 0 & 1 & 1 & 1 \\ \hline
        \rowcolor{gris}
        0 & + & 7  & 7  & & 0 & 0 & 1 & 0 & 0 \\ \hline
        0 & - & 7  & 249[-7] & & 0 & 0 & 1 & 1 & 1 \\ \hline
        \rowcolor{gris}
        0 & + & 255[-1] & 255[-1] & & 0 & 0 & 1 & 0 & 1 \\ \hline 
        0 & - & 255[-1] & 1  & & 0 & 0 & 1 & 0 & 0 \\ \hline
        \rowcolor{gris}
        128[-128] & + & 1 & 129[-127] & & 0 & 0 & 1 & 0 & 1 \\ \hline
        128[-128] & - & 1 & 127 & & 1 & 0 & 1 & 0 & 0 \\ \hline
        \rowcolor{gris}
        128[-128] & + & 128[-128] & 0  & & 1 & 1 & 0 & 1 & 0 \\ \hline
        128[-128] & - & 128[-128] & 0  & & 1 & 1 & 0 & 1 & 0 \\ \hline
        \rowcolor{gris}
        255[-1] & + &    1 & 0  &  & 1 & 1 & 0 & 0 & 0 \\ \hline
        255[-1] & - &    1 & 254[-2] &  & 1 & 0 & 0 & 0 & 1 \\ \hline
        \rowcolor{gris}
        255[-1] & + & 255[-1] & 254[-2] &  & 1 & 0 & 0 & 0 & 1 \\ \hline
        255[-1] & - & 255[-1] & 0  &  & 1 & 1 & 0 & 1 & 0 \\ \hline
        \rowcolor{gris}
        255[-1] & + &    0 & 255[-1] &  & 0 & 0 & 1 & 0 & 1 \\ \hline
        255[-1] & - &    0 & 255[-1] &  & 1 & 0 & 1 & 0 & 1 \\ \hline
        \rowcolor{gris}
        255[-1] & + &   10 & 9  &  & 1 & 0 & 1 & 0 & 0 \\ \hline
        255[-1] & - &   10 & 245[-11] &  & 1 & 0 & 1 & 0 & 1 \\ 
        \hline
    \end{tabular}
    \captionof{table}{Batería de pruebas aritméticas.}
    \label{bateria}
\end{figure}

\newpage