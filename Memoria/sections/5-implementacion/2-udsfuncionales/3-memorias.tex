La arquitectura dispone de memorias separadas para datos y para instrucciones.
Aunque ambas tendrán estructuras muy parecidas, cabe resaltar que no serán iguales,
siendo la principal diferencia el direccionamiento. Como se expuso anteriormente,
las memorias suelen
direccionar a byte. Sin embargo, por cuestiones de claridad y sencillez, la
memoria de instrucciones tendrá posiciones de 16 bits para que cada una pueda
contener íntegramente una instrucción. De esta forma, nos ahorramos tener que
desplazar a la izquierda usando un \textit{left shifter} para no perder un bit
de direccionamiento. Además, la estructura de la memoria gana en comprensión, ya
que el contador de programa incrementará de uno en uno para apuntar a la siguiente
instrucción. En esta sección se detallará cómo se han implementado los dos tipos
de memoria.

La memoria de datos será direccionable a byte por comodidad, ya que ese es el tamaño
de los registros. Si bien podría ser direccionable a menos, por ejemplo a cuatro
bits, ésto incomodaría y ralentizaría el sistema, ya que para cargar un dato
de memoria en registro o para guardar un dato de registro en memoria se
necesitarían dos accesos, a no ser que se reestructurase la memoria para
permitir varias lecturas o escrituras simultáneamente, lo cual aumentaría
considerablemente su complejidad. De la misma forma podría ser direccionable
a más, pero ésto sería utilizar recursos inútilmente, ya que dado el juego
de instrucciones diseñado no hay forma de cargar de, o guardar en memoria más
de ocho bits en la misma operación.


El diagrama de bloque de ambas memorias se puede ver en la figura~\ref{fig:memblock}
Su organización interna será de la siguiente forma, para una memoria
de 4 posiciones:
\begin{figure}[H]
    \centering
    \includesvg[scale=0.7]{images/5-implementacion/3-Mem/Mem4pos}
    \caption{Vista interna de una memoria de 4 posiciones. Las posiciones
        son de \textit{n} bits cada una.}
    \label{fig:mem4pos}
\end{figure}

Como VHDL es agnóstico a la tecnología de fabricación, para implementar las
celdas de memoria se han utilizado registros del tamaño necesario, en este caso
de 8 bits. Como se verá más adelante, la implementación de la memoria de
instrucciones es similar pero usando celdas de 16 bits. Los registros guardan
los datos introducidos en ellos desde \textit{data\_in}, pero sólo lo guardará
el registro seleccionado por la entrada \textit{dir}, que también se usará
para seleccionar el registro cuyos datos se enviarán a la salida \textit{data\_out}.
Además, las puertas \textit{AND} antes de la entrada de activación se encargan de
sincronizar las escrituras con el ciclo de reloj.


Las memorias se implementarán de dos maneras. La primera forma es implementarla
por bloque, utilizando los componentes que se ven en la figura~\ref{fig:mem4pos}
(un decodificador,
un determinado número de registros y puertas \textit{AND}, y un multiplexor).
Cuando se realiza de esta forma, el esquemático generado por Vivado es el
de la figura~\ref{fig:memgoodschem}
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/5-implementacion/3-Mem/Mem4posSchem.png}
    \caption{Esquemático generado por Vivado para una memoria realizada con bloques.}
    \label{fig:memgoodschem}
\end{figure}

Pero también puede realizarse de manera conductual, sin bloques. El esquemático
generado por Vivado en ese caso es el mostrado en la figura~\ref{fig:membadschem}:
\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{images/5-implementacion/3-Mem/MemBehavSchem.png}
    \caption{Esquemático generado por Vivado para una memoria realizada conductualmente.}
    \label{fig:membadschem}
\end{figure}

Debido a que la primera forma es un buen ejercicio de aprendizaje, y al ser ese el
principal objetivo de este proyecto, resultaba incoherente no realizarla y
simularla, al menos para demostrar cómo se realiza. Sin embargo, por comodidad
y rapidez, al integrar todo el sistema, se usó el módulo conductual.

Las baterías de pruebas son análogas para ambas formas, dando los mismos
resultados:

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/5-implementacion/3-Mem/MemSim.png}
    \caption{Simulación de las memorias.}
    \label{fig:simumem}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{images/5-implementacion/3-Mem/MemBehavSim.png}
    \caption{Simulación de las memorias (conductual).}
\end{figure}

En la simulación se puede observar cómo en los primeros cuatro ciclos se cargan los
valores del 0 al 3 en sus respectivas posiciones, y en los últimos cuatro ciclos éstos
valores únicamente se leen, pero no se escribe el 3 que se mantiene en la entrada de datos.
