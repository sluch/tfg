

![Unidad de Control](images/UnidadControl/UnidadControl.png)

# Opcodes

Inst | opcode | hex | dec
 :-: | :----: | :-: | :-:
 ADD | 0111   | 0x7 | 7
 SUB | 0101   | 0x5 | 5
 CMP | 0100   | 0x4 | 4
 AND | 0000   | 0x0 | 0
 OR  | 0001   | 0x1 | 1
 XOR | 0010   | 0x2 | 2
---- | ----   | --- | ---
 LD  | 1000   | 0x8 | 8
 LDI | 1001   | 0x9 | 9
 ST  | 1010   | 0xA | 10
 STI | 1011   | 0xB | 11
 MOV | 1100   | 0xC | 12
 JMP | 1110   | 0xE | 14
 BEQ | 1111   | 0xF | 15


# Tabla de señales de control vs. instrucciones

 SC \ Inst | Add | Sub | Cmp | And |  Or | Xor |  LD | LDI |  ST | STI | MOV | JMP | BEQ
---------- | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-:
PCSrc      |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  | 0/1 
RegWrite   |  1  |  1  |  0  |  1  |  1  |  1  |  1  |  1  |  0  |  0  |  1  |  0  |  0  
WDSrc0     |  0  |  0  |  -  |  0  |  0  |  0  |  1  |  1  |  -  |  -  |  0  |  -  |  -  
WDSrc1     |  0  |  0  |  -  |  0  |  0  |  0  |  0  |  1  |  -  |  -  |  1  |  -  |  -  
MemWrite   |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  1  |  0  |  0  |  0  
MemRead    |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  0  |  0  |  0  |  0  |  0  |  0  
dat_inSrc  |  -  |  -  |  -  |  -  |  -  |  -  |  -  |  -  |  0  |  1  |  -  |  -  |  -  
dirSrc     |  -  |  -  |  -  |  -  |  -  |  -  |  0  |  -  |  0  |  1  |  -  |  -  |  -  

# Explicación de cada señal de control
## PCSrc
Controla cómo modificamos el Program Counter. Tenemos dos formas: incrementarlo  
normalmente, sumándole 2* para ir a la siguiente instrucción de la memoria de  
instrucciones o incrementarlo de forma manual con las instrucciones `JMP` o `BEQ`.  
  
(*) 2 Porque el PC es un puntero a la memoria de instrucciones. Como cada instrucción  
tiene 2 bytes (16 bits), incrementando en 2 el PC, apuntamos a la siguiente instrucción.  
-> Esto es contando con que la memoria de instrucciones sea direccionable a byte,  
si es direccionable a 16 bits se tendría que incrementar en 1.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | PC = PC + 2       | El resto
1     | PC = dir          | JMP, BEQ

## RegWrite
Controla si escribimos a registro o no.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | Ignoramos lo que hay<br/>en la entrada WriteData | El resto
1     | Escribimos lo que hay en WriteData<br/>al registro señalado por WriteReg | CMP, ST, STI, JMP, BEQ

## WDSrc0 y WDSrc1 (WriteData Source)
Este campo es un poco más complejo. Es la misma señal de control, pero formada por dos bits.  
Controla desde dónde vamos a escribir al registro que sea. Como tenemos 4 posibles orígenes  
de la escritura a registro, con estos dos bits elegiremos desde dónde lo hacemos. Los cuatro  
posibles orígenes son:
 * La ALU - Por las instrucciones `ADD`, `SUB`, `AND`, `OR`, `XOR`
 * Registro Destino - Por la instrucción `MOV`
 * dat_out - Por la instrucción `LD`
 * Inmediato - Por la instrucción `LDI`

Valores temporales. Hay que estudiar más los valores porque tengo la impresión de que  
su elección puede condicionar la facilidad o dificultad del diseño de la UC.  
WDSrc1 | WDSrc0 | Instrucciones
:----: | :----: | :-------------
0      | 0      | ADD, SUB, AND, OR, XOR
0      | 1      | LD
1      | 0      | MOV
1      | 1      | LDI

## dat_inSrc
Controla lo que mandamos a la entrada `dat_in` de la memoria de datos. Esta entrada  
la usamos para escribir datos.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | En memoria cargamos<br/>lo que hay en `Rd` | ST
1     | En memoria cargamos<br/>el `inmediato` | STI


## dirSrc
Controla la fuente de la dirección a la que queremos escribir o de la que queremos  
leer en memoria de datos.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | La dirección de memoria<br/>la tomamos del `inmediato` | ST, LD
1     | La dirección de memoria<br/>la tomamos del registro `Rd` | STI

## MemWrite
Controla si escribimos a memoria o no.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | No escribimos a memoria | El resto
1     | Escribimos a memoria | ST, STI


## MemRead
Controla si leemos de memoria o no.

Valor | Descripción       | Instrucciones
:---: | :---------------- | :-----------:
0     | No leemos de memoria | El resto
1     | Leemos de memoria | LD

`LDI` no lee de memoria. Simplemente carga el valor dado en el campo `Inmediato`  
al registro dado en el campo `Rd`


![Señales de Control](images/UnidadControl/SeñalesControl.png)
