library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UAL is
    port (A, B        : in  STD_LOGIC_VECTOR (7 downto 0);
          ALUOp2      : in  STD_LOGIC;
          ALUOp1      : in  STD_LOGIC;
          ALUOp0      : in  STD_LOGIC;
          Res         : out STD_LOGIC_VECTOR (7 downto 0);
          ZF, OFF, PF : out STD_LOGIC;
          SF, CF      : out STD_LOGIC);
end UAL;

architecture Arch_UAL of UAL is

    component GenericAND
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericOR
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericXOR
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericMux2_1
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z    : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericXOREnabler
        generic (DataWidth : integer := 8);
        port (a : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z : in  STD_LOGIC;
              S : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component Adder8bit
        port (a, b : in  STD_LOGIC_VECTOR (7 downto 0);
              Cin  : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (7 downto 0);
              Cout : out STD_LOGIC);
    end component;


    signal ANDout, ORout, XORout                 : STD_LOGIC_VECTOR (7 downto 0);
    signal XOREnablerOut                         : STD_LOGIC_VECTOR (7 downto 0);
    signal CtrlMUX0out, CtrlMUX1out, CtrlMUX2out : STD_LOGIC_VECTOR (7 downto 0);
    signal AdderOut                              : STD_LOGIC_VECTOR (7 downto 0);
    signal ALUres                                : STD_LOGIC_VECTOR (7 downto 0);

    signal ResMSB, ResLSB : STD_LOGIC;

begin
    AandB : GenericAND
        generic map (DataWidth => 8)
        port map (a => A,
                  b => b,
                  S => ANDout);

    AorB : GenericOR
        generic map (DataWidth => 8)
        port map (a => A,
                  b => B,
                  S => ORout);

    AxorB : GenericXOR
        generic map (DataWidth => 8)
        port map (a => A,
                  b => B,
                  S => XORout);

    CtrlMux0 : GenericMux2_1
        generic map (DataWidth => 8)
        port map (a => ANDout,
                  b => ORout,
                  Z => ALUOp0,
                  S => CtrlMUX0out);

    CtrlMux1 : GenericMux2_1
        generic map (DataWidth => 8)
        port map (a => CtrlMux0out,
                  b => XORout,
                  Z => ALUOp1,
                  S => CtrlMux1out);

    XOREnabler : GenericXOREnabler
        generic map (DataWidth => 8)
        port map (a => B,
                  Z => ALUOp2,
                  S => XOREnablerOut);

    Adder : Adder8bit
        port map (a    => A,
                  b    => XOREnablerOut,
                  Cin  => ALUOp2,
                  S    => AdderOut,
                  Cout => CF);

    CtrlMux2 : GenericMux2_1
        generic map (DataWidth => 8)
        port map (a => CtrlMux1Out,
                  b => AdderOut,
                  Z => ALUOp0,
                  S => ALUres);

    ZF  <= '1' when ALURes = "00000000" else '0';

    PF  <= ALUres(0);
    SF  <= ALUres(7);
    OFF <= (not ALUres(7) and A(7) and B(7)) or   -- Negative result on positive inputs
           (ALures(7) and not A(7) and not B(7)); -- Positive result on negative inputs

    Res <= ALUres;


end Arch_UAL;
