
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BancoRegistros is
    port (ReadReg0 : in  STD_LOGIC_VECTOR (2 downto 0);
          ReadReg1 : in  STD_LOGIC_VECTOR (2 downto 0);
          RegSel   : in  STD_LOGIC_VECTOR (2 downto 0);
          WriteDat : in  STD_LOGIC_VECTOR (7 downto 0);
          WriteEn  : in  STD_LOGIC;
          ReadDat0 : out STD_LOGIC_VECTOR (7 downto 0);
          ReadDat1 : out STD_LOGIC_VECTOR (7 downto 0));
end BancoRegistros;

architecture Arch_BancoRegistros of BancoRegistros is
    component Decod3_8
        port (e  : in  STD_LOGIC_VECTOR (2 downto 0);
              en : in  STD_LOGIC;
              s  : out STD_LOGIC_VECTOR (7 downto 0));
    end component;

    component GenericRegBehav
        generic (DataWidth : integer := 8);
        port (en      : in  STD_LOGIC;
              dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericMux8_3
        generic (DataWidth : integer := 8);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              e, f, g, h : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (2 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));

    end component;

    constant DW : integer := 8;

    type v8 is array (natural range <>) of STD_LOGIC_VECTOR (7 downto 0);
    signal regOut : v8 (7 downto 0);

    signal d3_8_s : STD_LOGIC_VECTOR (7 downto 0);

begin

    d3_8 : Decod3_8
        port map (en => WriteEn,
                  e  => RegSel,
                  s  => d3_8_s);

    gen_regs : for I in 0 to 7 generate
    begin
        reg_x : GenericRegBehav
            generic map (DataWidth => DW)
            port map (dataIn  => WriteDat,
                      en      => d3_8_S(I),
                      dataOut => regOut(I));
    end generate gen_regs;

    mux0 : GenericMux8_3
        generic map (DataWidth => DW)
        port map (a => regOut(0),
                  b => regOut(1),
                  c => regOut(2),
                  d => regOut(3),
                  e => regOut(4),
                  f => regOut(5),
                  g => regOut(6),
                  h => regOut(7),
                  Z => ReadReg0,
                  S => ReadDat0);

    mux1 : GenericMux8_3
        generic map (DataWidth => DW)
        port map (a => regOut(0),
                  b => regOut(1),
                  c => regOut(2),
                  d => regOut(3),
                  e => regOut(4),
                  f => regOut(5),
                  g => regOut(6),
                  h => regOut(7),
                  Z => ReadReg1,
                  S => ReadDat1);


end Arch_BancoRegistros;
