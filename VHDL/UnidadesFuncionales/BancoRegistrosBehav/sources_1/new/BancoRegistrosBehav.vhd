
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BancoRegistrosBehav is
    port (
          ReadReg0 : in  STD_LOGIC_VECTOR (2 downto 0);
          ReadReg1 : in  STD_LOGIC_VECTOR (2 downto 0);
          RegSel   : in  STD_LOGIC_VECTOR (2 downto 0);
          WriteDat : in  STD_LOGIC_VECTOR (7 downto 0);
          WriteEn  : in  STD_LOGIC;
          ReadDat0 : out STD_LOGIC_VECTOR (7 downto 0);
          ReadDat1 : out STD_LOGIC_VECTOR (7 downto 0));
end BancoRegistrosBehav;

architecture Arch_BancoRegistrosBehav of BancoRegistrosBehav is
    type bancoReg is array (7 downto 0) of STD_LOGIC_VECTOR (7 downto 0);
    signal regs : bancoReg;
begin
    
    br : process
    begin
        ReadDat0 <= regs(to_integer(unsigned(ReadReg0)));
        ReadDat1 <= regs(to_integer(unsigned(Readreg1)));
        if WriteEn = '1' then
            regs(to_integer(unsigned(RegSel))) <= WriteDat;
            if ReadReg0 = RegSel then
                ReadDat0 <= WriteDat;
            end if;
            if ReadReg1 = RegSel then
                ReadDat1 <= WriteDat;
            end if;
        end if;
        wait for 1 ns;
    end process;
end Arch_BancoRegistrosBehav;
