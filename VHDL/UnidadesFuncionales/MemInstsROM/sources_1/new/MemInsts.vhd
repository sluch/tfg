
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MemInsts is
    port (clk  : in  STD_LOGIC;
          dir  : in  STD_LOGIC_VECTOR (7 downto 0);
          dout : out STD_LOGIC_VECTOR (15 downto 0));
end MemInsts;

architecture Arch_MemInsts of MemInsts is
    type rom_type is array (0 to 255) of STD_LOGIC_VECTOR (15 downto 0);
    
    constant mem : rom_type := (
        -- Inicializar todos los registros para que no haya UwUs en la simulación
        "1001000000000000", -- LDI 0, 0                            [9000]  0
        "1001000000000000", -- LDI 0, 0                            [9000]  1
        "1001001000000001", -- LDI 1, 1                            [9201]  2
        "1001010000000010", -- LDI 2, 0                            [9401]  3
        "1001011000000011", -- LDI 3, 3                            [9603]  4
        "1001100000000100", -- LDI 4, 4                            [9804]  5
        "1001101000000101", -- LDI 5, 233                          [9A05]  6
        "1001110000000110", -- LDI 6, 6                            [9C06]  7
        "1001111000000111", -- LDI 7, 7                            [9E07]  8

        "1001000000000000", -- LDI 0, 0       [i = 0]              [9000]  9
        "1001001000000001", -- LDI 1, 1       [j = 1]              [9201]  10
        "1001010000000000", -- LDI 2, 0       [aux = 0]            [9400]  11
        "1001101011101001", -- LDI 5, 233                          [9AE9]  12
        "0100000101010000", -- CMP 5, 2       [aux == 233?]        [4150]  13
        "1111000000001001", -- BEQ 9          [ yes -> start over] [F000]  14
        "0111010000001000", -- ADD RC, RA, RB [aux = i + j]        [7408]  15
        "1100000001000000", -- MOV RA, RB     [i = j]              [C040]  16
        "1100001010000000", -- MOV RB, RC     [j = aux]            [C280]  17
        "1110000000001101", -- JMP 13         [GOTO (CMP 5, 2)]    [E004]  18
        others => "0011000000000000"
    );

begin

	dout <= mem(TO_INTEGER(UNSIGNED(dir)));


end Arch_MemInsts;
