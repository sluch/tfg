
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UC is
    port (opcode    : in  STD_LOGIC_VECTOR (3 downto 0);
          FZ        : in  STD_LOGIC;
          PCSrc     : out STD_LOGIC;
          RegWrite  : out STD_LOGIC;
          WDSrc0    : out STD_LOGIC;
          WDSrc1    : out STD_LOGIC;
          MemWrite  : out STD_LOGIC;
          RR0Src    : out STD_LOGIC;
          dinSrc    : out STD_LOGIC;
          dirSrc    : out STD_LOGIC;
          ALUOp0    : out STD_LOGIC;
          ALUOp1    : out STD_LOGIC;
          ALUOp2    : out STD_LOGIC;
          ALUEn     : out STD_LOGIC);
end UC;

architecture Arch_UC of UC is
    component Decod4_16
        port (e  : in  STD_LOGIC_VECTOR (3 downto 0);
              en : in  STD_LOGIC;
              S  : out STD_LOGIC_VECTOR (15 downto 0));
    end component;
    
    component Decod3_8
        port (e  : in  STD_LOGIC_VECTOR (2 downto 0);
              en : in  STD_LOGIC;
              s  : out STD_LOGIC_VECTOR (7 downto 0));
    end component;
    
    signal d4_16_s : STD_LOGIC_VECTOR (15 downto 0);
    signal d3_8_s  : STD_LOGIC_VECTOR (7 downto 0);
    signal d3_8_en : STD_LOGIC;
    
    signal LD, LDI, ST, STI, MOV, JMP, BEQ   : STD_LOGIC;
    signal ADD, SUB, CMP, AND_S, OR_S, XOR_S : STD_LOGIC;
    
begin

    d4_16 : Decod4_16 port map (en => '1',
                                e  => opcode,
                                S  => d4_16_s);

    LD  <= d4_16_s(8);
    LDI <= d4_16_s(9);
    ST  <= d4_16_s(10);
    STI <= d4_16_s(11);
    MOV <= d4_16_s(12);
    JMP <= d4_16_s(14);
    BEQ <= d4_16_s(15);
    
    d3_8_en <= (d4_16_s(0) or d4_16_s(1) or d4_16_s(2) or 
                d4_16_s(4) or d4_16_s(5) or d4_16_s(7));
    

    d3_8 : Decod3_8 port map (en => d3_8_en,
                              e  => opcode(2 downto 0),
                              S  => d3_8_s);

    AND_S <= d3_8_s(0);
    OR_S  <= d3_8_s(1);
    XOR_S <= d3_8_s(2);
    CMP   <= d3_8_s(4);
    SUB   <= d3_8_s(5);
    ADD   <= d3_8_s(7);
    
    PCSrc     <= (JMP or (BEQ and FZ));
    RegWrite  <= (ADD or SUB or AND_S or OR_S or XOR_S or LD or LDI or MOV);
    WDSrc0    <= (LD or LDI);
    WDSrc1    <= (LDI or MOV);
    MemWrite  <= (ST or STI);
    RR0Src    <= (ST or STI);
    dinSrc    <= (STI);
    dirSrc    <= (STI);
    
    ALUOp0    <= (OR_S or ADD or SUB or CMP);
    ALUOp1    <= (XOR_S or ADD);
    ALUOp2    <= (SUB or CMP);
    
    ALUEn     <= (ADD or SUB or CMP or AND_s or OR_s or XOR_s);
    
end Arch_UC;
