
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all; -- to_unsigned()

entity bm_Adder4bit is
end bm_Adder4bit;

architecture benchmark of bm_Adder4bit is
    component Adder4bit
        port (a, b : in  STD_LOGIC_VECTOR (3 downto 0);
              Cin  : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (3 downto 0);
              Cout : out STD_LOGIC);
    end component;
    
    signal a_s, b_s, S_s : STD_LOGIC_VECTOR (3 downto 0);
    signal Cin_s, Cout_s : STD_LOGIC;
    
begin
    benchmark : Adder4bit port map (a    => a_s,
									b    => b_s,
									S    => S_s,
                                    Cin  => Cin_s,
									Cout => Cout_s);
                                    
    bm_proc : process
    begin
        for I in 0 to (2 ** a_s'length) - 1 loop
            for J in 0 to (2 ** b_s'length) - 1 loop
                for K in STD_LOGIC range '0' to '1' loop                
                    Cin_s <= K;
                    a_s   <= STD_LOGIC_VECTOR(to_unsigned(I, a_s'length));
                    b_s   <= STD_LOGIC_VECTOR(to_unsigned(J, b_s'length));
                    wait for 2ns;
                end loop;
            end loop;
        end loop;
    end process;

end benchmark;
