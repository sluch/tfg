
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bm_FullAdder is
end bm_FullAdder;

architecture benchmark_FullAdder of bm_FullAdder is
    component FullAdder
    port (a, b, Cin : in  STD_LOGIC;
          Cout, S   : out STD_LOGIC);
    end component;

    signal a_s, b_s, Cin_s, Cout_s, S_s : STD_LOGIC;
    
begin
    benchmark : FullAdder port map(a    => a_s,
                                   b    => b_s,
                                   Cin  => Cin_s,
                                   Cout => Cout_s,
                                   S    => S_s);

    bm_proc : process
    begin
            Cin_s <= '0';
            a_s   <= '0';
            b_s   <= '0';
        wait for 10ns;
            a_s   <= '0';
            b_s   <= '1';
        wait for 10ns;
            a_s   <= '1';
            b_s   <= '0';
        wait for 10ns;
            a_s   <= '1';
            b_s   <= '1';
            
        wait for 10ns;
            Cin_s <= '1';
            a_s   <= '0';
            b_s   <= '0';
        wait for 10ns;
            a_s   <= '0';
            b_s   <= '1';
        wait for 10ns;
            a_s   <= '1';
            b_s   <= '0';
        wait for 10ns;
            a_s   <= '1';
            b_s   <= '1';
        wait for 10ns;
    end process;

end benchmark_FullAdder;
