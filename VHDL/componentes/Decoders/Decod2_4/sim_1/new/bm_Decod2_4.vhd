
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bm_Decod2_4 is
end bm_Decod2_4;

architecture benchmark_Decod2_4 of bm_Decod2_4 is
    component Decod2_4
		port (e  : in  STD_LOGIC_VECTOR (1 downto 0);
			  en : in  STD_LOGIC; 
			  S  : out STD_LOGIC_VECTOR (3 downto 0));
    end component;
    
    signal en_s : STD_LOGIC;
    signal e_s  : STD_LOGIC_VECTOR (1 downto 0);
    signal S_s  : STD_LOGIC_VECTOR (3 downto 0);
    
begin
    benchmark : Decod2_4 port map(en   => en_s,
                                  e(1) => e_s(1),
                                  e(0) => e_s(0),
                                  s(0) => S_s(0),
                                  s(1) => S_s(1),
                                  s(2) => S_s(2),
                                  s(3) => S_s(3));

    bm_proc : process
        variable tmp : STD_LOGIC := '0';
    begin
        en_s <= tmp;
            e_s(1) <= '0';
            e_s(0) <= '0';
        wait for 10ns;
            e_s(1) <= '0';
            e_s(0) <= '1';
        wait for 10ns;
            e_s(1) <= '1';
            e_s(0) <= '0';
        wait for 10ns;
            e_s(1) <= '1';
            e_s(0) <= '1';
        wait for 10ns;
        tmp := not tmp;
        
    end process;

end benchmark_Decod2_4;
