
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_Decod3_8 is
end bm_Decod3_8;

architecture benchmark_Decod3_8 of bm_Decod3_8 is
    component Decod3_8
        port (e  : in  STD_LOGIC_VECTOR (2 downto 0);
              en : in  STD_LOGIC;
              s  : out STD_LOGIC_VECTOR (7 downto 0));
    end component;
    
    signal en_s : STD_LOGIC;
    signal e_s  : STD_LOGIC_VECTOR (2 downto 0);
    signal s_s  : STD_LOGIC_VECTOR (7 downto 0);
    
begin
    benchmark : Decod3_8 port map(en => en_s,
                                  e  => e_s,
                                  s  => s_s);
                                  
    bm_proc : process
        variable tmp  : STD_LOGIC_VECTOR(0 to 2);
    begin
        for J in STD_LOGIC range '0' to '1' loop
            en_s <= J;
            for I in 0 to 7 loop
                tmp := STD_LOGIC_VECTOR(TO_UNSIGNED(I, TMP'length));
                e_s(2) <= tmp(0);
                e_s(1) <= tmp(1);
                e_s(0) <= tmp(2);
                wait for 2ns;
            end loop;
        end loop;
    end process;

end benchmark_Decod3_8;
