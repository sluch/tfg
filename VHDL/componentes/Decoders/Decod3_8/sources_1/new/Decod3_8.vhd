
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decod3_8 is
    port (e  : in  STD_LOGIC_VECTOR (2 downto 0);
          en : in  STD_LOGIC;
          s  : out STD_LOGIC_VECTOR (7 downto 0));
end Decod3_8;

architecture Arch_Decod3_8 of Decod3_8 is
begin
    s(0) <= (en) and ((not e(2)) and (not e(1)) and (not e(0))); -- 0 0 0
    s(1) <= (en) and ((not e(2)) and (not e(1)) and (    e(0))); -- 0 0 1
    s(2) <= (en) and ((not e(2)) and (    e(1)) and (not e(0))); -- 0 1 0
    s(3) <= (en) and ((not e(2)) and (    e(1)) and (    e(0))); -- 0 1 1
    s(4) <= (en) and ((    e(2)) and (not e(1)) and (not e(0))); -- 1 0 0
    s(5) <= (en) and ((    e(2)) and (not e(1)) and (    e(0))); -- 1 0 1
    s(6) <= (en) and ((    e(2)) and (    e(1)) and (not e(0))); -- 1 1 0
    s(7) <= (en) and ((    e(2)) and (    e(1)) and (    e(0))); -- 1 1 1
end Arch_Decod3_8;
