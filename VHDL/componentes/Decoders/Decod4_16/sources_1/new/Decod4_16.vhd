
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decod4_16 is
    port (e  : in  STD_LOGIC_VECTOR (3 downto 0);
          en : in  STD_LOGIC;
          S  : out STD_LOGIC_VECTOR (15 downto 0));
end Decod4_16;

architecture Arch_Decod4_16 of Decod4_16 is
    component Decod2_4 is
        port (e  : in  STD_LOGIC_VECTOR (1 downto 0);
              en : in  STD_LOGIC;
              S  : out STD_LOGIC_VECTOR (3 downto 0));
    end component;
    
    signal decodS_s : STD_LOGIC_VECTOR(3 downto 0);
    
begin

    decod0 : Decod2_4 port map(en => en,
                               e  => e(3 downto 2),
                               S  => decodS_s);

    decod1 : Decod2_4 port map(en => decodS_s(0),
                               e  => e(1 downto 0),
                               S  => S(3 downto 0));
                               
    decod2 : Decod2_4 port map(en => decodS_s(1),
                               e  => e(1 downto 0),
                               S  => S(7 downto 4));
                               
    decod3 : Decod2_4 port map(en => decodS_s(2),
                               e  => e(1 downto 0),
                               S  => S(11 downto 8));
                               
    decod4 : Decod2_4 port map(en => decodS_s(3),
                               e  => e(1 downto 0),
                               S  => S(15 downto 12));
                                                      
end Arch_Decod4_16;
