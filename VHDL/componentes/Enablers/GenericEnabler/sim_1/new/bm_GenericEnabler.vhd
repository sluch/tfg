
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericEnabler is
end bm_GenericEnabler;

architecture benchmark of bm_GenericEnabler is
    component GenericEnabler
        generic (DataWidth : integer);
        port (a : in  STD_LOGIC_VECTOR (DataWidth -1 downto 0);
              Z : in  STD_LOGIC;
              S : out STD_LOGIC_VECTOR (DataWidth -1 downto 0));
    end component;

    constant DW : integer := 4;

    signal a_s, S_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal Z_s      : STD_LOGIC;

begin

    benchmark : GenericEnabler
    generic map (DataWidth => DW)
    port map (a => a_s,
              Z => Z_s,
              S => S_s);


    bm_proc : process
    begin
        for J in 0 to (2 ** a_s'length) - 1 loop
            for I in STD_LOGIC range '0' to '1' loop
                Z_s <= I;
                a_s <= STD_LOGIC_VECTOR(to_unsigned(J, a_s'length));
                wait for 2ns;
            end loop;
        end loop;
    end process;

end benchmark;
