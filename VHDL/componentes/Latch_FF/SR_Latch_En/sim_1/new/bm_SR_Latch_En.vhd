
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bm_SR_Latch_En is
end bm_SR_Latch_En;

architecture benchmark_SR_Latch_En of bm_SR_Latch_En is
    component SR_Latch_en
		port (r, s, E : in  STD_LOGIC;
			  q, qn   : out STD_LOGIC);
    end component;
    
    signal r_s, s_s, E_s, q_s, qn_s : STD_LOGIC;
    
begin
    benchmark : SR_Latch_En port map (r  => r_s,
                                      s  => s_s,
                                      E  => E_s,
                                      q  => q_s,
                                      qn => qn_s);
                                      
    bm_proc : process
    begin              -- Q(t+1) = 1
            E_s <= '1';
            r_s <= '0';
            s_s <= '1';
        wait for 10ns; -- Q(t+1) = Q(t)
            E_s <= '1';
            r_s <= '0';
            s_s <= '0';
            
        wait for 10ns; -- Q(t+1) = 0
            E_s <= '1';
            r_s <= '1';
            s_s <= '0';
        wait for 10ns; -- Q(t+1) = Q(t)
            E_s <= '0';
            r_s <= '0';
            s_s <= '1';
            
        wait for 10ns; -- Q(t+1) = 0 (Prohibited)
            E_s <= '1';
            r_s <= '1';
            s_s <= '1';
        wait for 10ns; -- Q(t+1) = Q(t)
            E_s <= '0';
            r_s <= '0';
            s_s <= '0';
        wait for 10ns;
    end process;

end benchmark_SR_Latch_En;
