
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SR_Latch_En is
    port (r, s, E : in  STD_LOGIC;
          q, qn   : out STD_LOGIC);
end SR_Latch_En;

architecture Arch_SR_Latch_En of SR_Latch_En is
    signal q_s  : STD_LOGIC;
    signal qn_s : STD_LOGIC;
begin
    process (r, s, E)
    begin
        if E = '1' then
          --if    r = '0' and s = '0' then -- Keep previous state of q and qn
               if r = '0' and s = '1' then -- q = 1
                q_s  <= '1';
                qn_s <= '0';
            elsif r = '1' and s = '0' then -- q = 0
                q_s  <= '0';
                qn_s <= '1';
            elsif r = '1' and s = '1' then -- Prohibited state
                q_s  <= '0';
                qn_s <= '0';
            end if;
        elsif E = '0' then
            -- Keep previous state of q and qn
        end if;
    end process;
    q  <= q_s;
    qn <= qn_s;
end Arch_SR_Latch_En;
