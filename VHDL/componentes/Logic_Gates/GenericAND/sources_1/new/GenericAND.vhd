
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericAND is
    generic (DataWidth : integer := 8);
    port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericAND;

architecture Arch_GenericAND of GenericAND is
begin
    gen : for I in DataWidth - 1 downto 0 generate
        S(I) <= (a(I) and b(I));
    end generate;

end Arch_GenericAND;
