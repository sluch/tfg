
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericOR is
    generic (DataWidth : integer := 8);
    port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericOR;

architecture Arch_GenericOR of GenericOR is
begin
    gen : for I in DataWidth - 1 downto 0 generate
        S(I) <= (a(I) or b(I));
    end generate;

end Arch_GenericOR;
