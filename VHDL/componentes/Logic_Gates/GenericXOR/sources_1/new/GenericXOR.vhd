
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericXOR is
    generic (DataWidth : integer := 8);
    port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericXOR;

architecture Arch_GenericXOR of GenericXOR is
begin
    gen : for I in DataWidth - 1 downto 0 generate
        S(I) <= (a(I) xor b(I));
    end generate;

end Arch_GenericXOR;
