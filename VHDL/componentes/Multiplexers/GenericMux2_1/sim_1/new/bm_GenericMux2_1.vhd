
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericMux2_1 is
end bm_GenericMux2_1;

architecture benchmark of bm_GenericMux2_1 is
    component GenericMux2_1
        generic (DataWidth : integer);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z    : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    constant DW : integer := 3;

    signal a_s, b_s, S_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal Z_s           : STD_LOGIC;

begin
    benchmark : GenericMux2_1
    generic map (DataWidth => DW)
    port map (a => a_s,
              b => b_s,
              Z => Z_s,
              S => S_s);

    bm_proc : process
    begin
        for K in 0 to (2 ** b_s'length) - 1 loop
            for J in 0 to (2 ** b_s'length) - 1 loop
                for I in STD_LOGIC range '0' to '1' loop
                    Z_s <= I;
                    a_s <= STD_LOGIC_VECTOR(to_unsigned(J, a_s'length));
                    b_s <= STD_LOGIC_VECTOR(to_unsigned(K, b_s'length));
                    wait for 2ns;
                end loop;
            end loop;
        end loop;
    end process;

end benchmark;
