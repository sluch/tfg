library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericMux8_3 is
    generic (DataWidth : integer := 8);
    port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          e, f, g, h : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          Z          : in  STD_LOGIC_VECTOR (2 downto 0);
          S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericMux8_3;

architecture Arch_GenericMux8_3 of GenericMux8_3 is
    component GenericMux4_2
        generic (DataWidth : integer := 8);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (1 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericMux2_1
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z    : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    signal m1out_s, m2out_s : STD_LOGIC_VECTOR (DataWidth - 1 downto 0);

begin
    w0 : GenericMux4_2
        generic map (DataWidth => DataWidth)
        port map (a => a,
                  b => c,
                  c => e,
                  d => g,
                  Z => Z(2 downto 1),
                  S => m1out_s);

    w1 : GenericMux4_2
        generic map (DataWidth => DataWidth)
        port map (a => b,
                  b => d,
                  c => f,
                  d => h,
                  Z => Z(2 downto 1),
                  S => m2out_s);

    w2 : GenericMux2_1
        generic map (DataWidth => DataWidth)
        port map (a => m1out_s,
                  b => m2out_s,
                  Z => Z(0),
                  S => S);

end Arch_GenericMux8_3;
