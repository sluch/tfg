
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericReg is
end bm_GenericReg;

architecture benchmark_GenericReg of bm_GenericReg is
    component GenericReg
        generic (DataWidth : integer);
        port (en      : in  STD_LOGIC;
              dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    constant DW : integer := 4;

    signal en_s          : STD_LOGIC;
    signal din_s, dout_s : STD_LOGIC_VECTOR (DW - 1 downto 0);

begin
    benchamrk : GenericReg
        generic map (DataWidth => DW)
        port map (En      => en_s,
                  dataIn  => din_s,
                  dataout => dout_s);
    
    bm_proc : process
        variable tmp : STD_LOGIC_VECTOR (DW downto 0);
        variable env : STD_LOGIC := '1';
    begin
        wait for 10ns;
        en_s <= '0';
        din_s <= "1111";
        for I in 0 to (2 ** DW) - 1 loop
            en_s  <= env;
            din_s <= STD_LOGIC_VECTOR(to_unsigned(I, DW));
            env   := not env;
            wait for 2ns;
        end loop;
    end process;

end benchmark_GenericReg;