
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericReg is
    generic (DataWidth : integer := 8);
    port (en      : in  STD_LOGIC;
          dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericReg;

architecture Arch_GenericReg of GenericReg is
    component SR_Latch_En
        port (r, s, E : in  STD_LOGIC;
              q, qn   : out STD_LOGIC);
    end component;

    signal r_s : STD_LOGIC_VECTOR (DataWidth - 1 downto 0);

begin
    r_s <= not dataIn;

    gen_sr : for I in DataWidth - 1 downto 0 generate
        sr_x : SR_Latch_En port map (E => en,
                                     r => r_s(i),
                                     s => dataIn(i),
                                     q => dataOut(i));
    end generate;

end Arch_GenericReg;
