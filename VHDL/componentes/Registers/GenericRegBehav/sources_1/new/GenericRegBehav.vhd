
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericRegBehav is
    generic (DataWidth : integer := 8);
    port (en      : in  STD_LOGIC;
          dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0) := (others=>'0'));
end GenericRegBehav;

architecture Arch_GRB of GenericRegBehav is
    signal dout_s : STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
    constant undefined : STD_LOGIC_VECTOR (DataWidth - 1 downto 0) := (others => 'U');
begin
    
    process (en)
    begin
        if (rising_edge(en)) then
            if (dataIn = undefined) then
            else
                dataOut <= dataIn;
            end if;
        end if;
    end process;
    
    
end Arch_GRB;
